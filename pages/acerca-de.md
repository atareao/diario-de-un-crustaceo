---
title: Acerca de
date: 2024-04-25
subject:
    - atareao
    - rust
    - crustáceo
    - rustacean
    - linux
    - docker
---
Mi nombre es Lorenzo, aunque normalmente me puedes encontrar en redes como *atareao*.

Este proyecto nación en abril de 2024, empujado por [Alberto](https://www.papagriki.es).

