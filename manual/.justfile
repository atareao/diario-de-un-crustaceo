ci:
    #!/bin/bash
    image=$(find ~/Imágenes/ -type f -name "*.jpg" -printf "%T@ %p\n" | sort -n | cut -d' ' -f2 | tail -n1)
    mv $image ./images/
