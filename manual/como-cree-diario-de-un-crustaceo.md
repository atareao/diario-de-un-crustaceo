#

* crear el repositorio en GitLab

![crear el repositorio en GitLab](./images/2024-04-24T06-27-39.jpg)

Primeros pasos


```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:atareao/diario-de-un-crustaceo.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```


Mi primer error

```bash
$ git push -u origin main
remote:
remote: ========================================================================
remote:
remote: ERROR: This deploy key does not have write access to this project.

remote:
remote: ========================================================================
remote:
fatal: No se pudo leer del repositorio remoto.

Por favor asegúrate de que tengas los permisos de acceso correctos
y que el repositorio exista.
```

En `Configuración > Repositorio` Seleccionar `Desplegar claves` y añades la tuya en `Add new key`

![Desplegar claves](./images/2024-04-24T06-43-25.jpg)

En el caso de que la tengas, lo que debes hacer es habilitarla y darle permisos de escritura, como en el siguiente ejemplo,

![claves](./images/2024-04-24T06-44-58.jpg)

![habilitar](./images/2024-04-24T06-46-20.jpg)

Y permisos des escritura

![permisos](./images/2024-04-24T06-47-02.jpg)

### Crear un token de despliegue

En `Configuración > Tokens de acceso` haz clic en `Add new token`

![add new token](./images/2024-04-24T06-50-06.jpg)

Con las siguientes características,

* Token name: `GIT_PUSH_TOKEN`
* Fecha de vencimiento: `` (vacío)
* Role: `maintainer`
* Scopes: `read_repository` y `write_repository`

![creacion de tocken](./images/2024-04-24T06-52-09.jpg)

Copia el valor del token `glpat-XXXXXXXXXXXXXXXXXX`

Y en `Configuración > CI/CD` despliega `Variables` y añade las siguientes variables,

* `GIT_PUSH_TOKEN` con el valor que acabas de copiar
* `RUST_LOG` con el valor `DEBUG`

![creación de GIT_PUSH_TOKEN](./images/2024-04-24T06-56-05.jpg)

Fíjate en la captura de pantalla que la variable está *enmascarada*

### Configure pipeline

El siguiente paso es configurar la pipeline, selecciona `Build > pipeline editor` y pega lo siguiente,

```bash
# The Docker image that will be used to build your app
image:
  name: atareao/iapodcast:0.1.0
  entrypoint: [""]

variables:
  GIT_DEPTH: 1                               # Create a shallow copy
  BRANCH_NAME: "main"                       # Name of the branch to modify
  BOT_NAME: "GitLab Runner Bot"              # Bot's name that appears in the commit log
  BOT_EMAIL: "gitlab-runner-bot@example.net" # Bot's email, not really important
  COMMIT_MESSAGE: "Commit from runner "      # Part of the commit message


.modify: &modify |
  echo "Start modify"
  /app/iapodcast

.push: &push |
  echo "Start push"
  git status
  lines=$(git status -s | wc -l)
  if [ $lines -gt 0 ];then
    echo "committing"
    git config --global user.name "${BOT_NAME}"
    git config --global user.email "${BOT_EMAIL}"
    git add .
    git commit -m "${COMMIT_MESSAGE} ${CI_RUNNER_ID}"
    echo "git push -o ci.skip 'https://whatever:${GIT_PUSH_TOKEN}@${CI_REPOSITORY_URL#*@}' ${BRANCH_NAME}"
    git push -o ci.skip "https://whatever:${GIT_PUSH_TOKEN}@${CI_REPOSITORY_URL#*@}" $BRANCH_NAME
  else
    echo "no change, nothing to commit"
  fi

pages:
  stage: deploy
  only:
    - schedules
  before_script:
    - apk add --update --no-cache make git bash
    - git fetch
    - git checkout $BRANCH_NAME
    - cd $CI_PROJECT_DIR
  script:
    - *modify
    - *push
  artifacts:
    paths:
    - public/
```

### Crear los directorios

* `../assets/`
* `../episodes/`
* `../public/`
* `../templates/`

copiar y modificar el archivo `config.yml`

### Crear la pipeline

`Build > Pipeline schedules`

![Pipeline schedules](./images/2024-04-24T07-11-01.jpg)

Programar un nuevo pipeline

![Programar un nuevo pipeline](./images/2024-04-24T07-13-16.jpg)

Dale al play

![Dale al play](./images/2024-04-24T07-14-02.jpg)
